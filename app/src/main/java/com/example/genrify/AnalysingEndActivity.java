package com.example.genrify;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class AnalysingEndActivity extends BaseActivity {
    private static final String TAG = "AnalyseEnd";
    private static final DecimalFormat decimalFormat = new DecimalFormat("#,##0.00");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.analysing_end);

        String[] genders = getIntent().getStringArrayExtra("genres");
        String[] percents = getIntent().getStringArrayExtra("probas");

        TextView mainResult = findViewById(R.id.tvAnalyseMainResult);
        String oldString = mainResult.getText().toString();
        Float percent = Float.parseFloat(percents[0]) * 100;
        mainResult.setText(oldString + " " + genders[0].toLowerCase() + " ( " + decimalFormat.format(percent) + "% )");
        TextView otherResults = findViewById(R.id.tvAnalysingOtherResults);
        String otherResultsString = otherResults.getText().toString();
        for (int i = 1; i < genders.length; i++) {
            percent = Float.parseFloat(percents[1]) * 100;
            otherResultsString += "\n " + genders[i].toLowerCase() + " ( " + decimalFormat.format(percent) + "% )";

        }
        otherResults.setText(otherResultsString);
        getBtn(R.id.validAnalysing).setOnClickListener(view -> {
            saveMp3(genders[0], getApplicationContext());
            Intent i = new Intent(this, MainActivity.class);
            startActivity(i);
        });
        if(this.isCurrentUserLogged()){
            getBtn(R.id.contestAnalysing).setVisibility(View.VISIBLE);
            getBtn(R.id.contestAnalysing).setOnClickListener(view -> {
                Intent i = new Intent(this, ContestActivity.class);
                startActivity(i);
            });
        }
    }

    private Button getBtn(int id) {
        return (Button) findViewById(id);
    }

    public static void saveMp3 (String mainGender, Context applicationContext) {
        String mp3PickedName = MainActivity.mp3PickedName;
        try {
            InputStream mp3PickedInputStream = applicationContext.getContentResolver().openInputStream(MainActivity.mp3PickedUri);
            mainGender = mainGender.toLowerCase();
            try {
                File dir = new File(applicationContext.getFilesDir(), mainGender);
                if (!dir.exists()) {
                    dir.mkdir();
                }
                if(mp3PickedName.equals("recordedaudio.mp3")){
                    mp3PickedName = new SimpleDateFormat("'record_'yyyyMMdd'_'HHmmss'.mp3'").format(new Date());
                }
                File file = new File(dir.getAbsolutePath() + "/" + mp3PickedName);
                String text = applicationContext.getResources().getString(R.string.movingFileInFolder, mp3PickedName, mainGender.toLowerCase());
                Toast.makeText(applicationContext, text, Toast.LENGTH_SHORT).show();
                if (!file.exists()) {
                    file.createNewFile();
                }

                FileOutputStream fos = new FileOutputStream(file);
                byte[] buf = new byte[mp3PickedInputStream.available()];
                int len;
                while ((len = mp3PickedInputStream.read(buf)) > 0) {
                    fos.write(buf, 0, len);
                }
                fos.close();
                mp3PickedInputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

}

