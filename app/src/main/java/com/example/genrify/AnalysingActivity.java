package com.example.genrify;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import be.tarsos.dsp.AudioDispatcher;
import be.tarsos.dsp.AudioEvent;
import be.tarsos.dsp.AudioProcessor;
import be.tarsos.dsp.io.TarsosDSPAudioFormat;
import be.tarsos.dsp.io.UniversalAudioInputStream;
import be.tarsos.dsp.io.android.AndroidFFMPEGLocator;
import be.tarsos.dsp.mfcc.MFCC;

public class AnalysingActivity extends BaseActivity {

    private static final String TAG = "AnalyseAct";
    public static String mfcctoshare;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AnalysingActivity context = this;
        setContentView(R.layout.analysing);
        Runnable runnable = () -> {
            try {
                context.onMFCC();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        };
        Thread thread = new Thread(runnable);
        thread.start();
    }

    private void onMFCC() throws FileNotFoundException {

        int sampleRate = 22050;
        int samplePerFrame = 512;
        int bufferOverlap = 0;
        AnalysingActivity context = this;

        new AndroidFFMPEGLocator(this);

        InputStream inStream = getContentResolver().openInputStream(MainActivity.mp3PickedUri);

        TarsosDSPAudioFormat audioFormat = new TarsosDSPAudioFormat(
                sampleRate,
                16, // sampleSizeInBits
                1, // channels
                true, // signed
                true // bigEndian
        );
        UniversalAudioInputStream audioInputStream = new UniversalAudioInputStream(inStream, audioFormat);
        final AudioDispatcher dispatcher = new AudioDispatcher(audioInputStream, samplePerFrame, bufferOverlap);
        final MFCC mfcc = new MFCC(
                samplePerFrame,
                sampleRate,
                20, // amountOfCepstrumCoef : number of mfccs for each frame
                30, // amountOfMelFilters
                133.3334F, // lowerFilterFreq : lowest frequency
                (float) sampleRate / 2.0F // upperFilterFreq : highest frequency
        );

        ArrayList<Float> mfccArr = new ArrayList<>();
        dispatcher.addAudioProcessor(mfcc);
        dispatcher.addAudioProcessor(new AudioProcessor() {
            @Override
            public void processingFinished() {

                RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                Map<String, String> jsonParams = new HashMap<String, String>();

                Gson gson = new Gson();
                // Convert numbers array into JSON string.
                String numbersJson = gson.toJson(mfccArr);

                jsonParams.put("mfcc", numbersJson);
                JSONArray params = new JSONArray();
                try {
                    params.put(0, mfccArr);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                JsonArrayRequest postRequest = new JsonArrayRequest(Request.Method.POST, urlGetGenre, params,
                        response -> {
                            try {
                                String[] genres = new String[2];
                                String[] probas = new String[2];
                                for (int i = 0; i < response.length(); i++) {
                                    JSONObject jsonObject = response.getJSONObject(i);
                                    genres[i] = jsonObject.getString("genre");
                                    probas[i] = jsonObject.getString("prob");
                                    Intent in = new Intent(context, AnalysingEndActivity.class);
                                    in.putExtra("genres",genres);
                                    in.putExtra("probas",probas);
                                    startActivity(in);
                                }
                                AnalysingActivity.mfcctoshare = numbersJson;
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        },
                        error -> {
                            Log.d(TAG, error.toString());
                            Intent in = new Intent(context, MainActivity.class);
                            in.putExtra("ErrorAPI","API Request Error");
                            startActivity(in);
                        }
                ) {
                    @Override
                    public Map<String, String> getHeaders() {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        headers.put("Content-Type", "application/json; charset=utf-8");
                        return headers;
                    }
                };
                postRequest.setRetryPolicy(new DefaultRetryPolicy(50000, 5, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                requestQueue.add(postRequest);

            }

            @Override
            public boolean process(AudioEvent audioEvent) {
                // breakpoint or logging to console doesn't enter function
                for(Float i: audioEvent.getFloatBuffer()){
                    mfccArr.add(i);
                }
                return true;
            }
        });

        dispatcher.run();

    }


}

