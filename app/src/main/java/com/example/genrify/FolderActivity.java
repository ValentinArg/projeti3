package com.example.genrify;

import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.TextView;

import java.io.File;

public class FolderActivity extends BaseActivity {

    private String folderName;
    private MediaPlayer mp;
    private Button stopButton;
    private Button currentButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mp = new MediaPlayer();
        setContentView(R.layout.folder);
        TextView title = findViewById(R.id.tvCredentialsAndRole);
        this.folderName = getIntent().getStringExtra("text");
        title.setText(folderName.toUpperCase());
        SearchView search = findViewById(R.id.searchFolder);
        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                updateUi(newText);
                return true;
            }
        });
        String filter = search.getQuery().toString();
        stopButton = findViewById(R.id.stopMusic);
        stopButton.setVisibility(View.INVISIBLE);
        stopButton.setOnClickListener(view -> {
            if (mp != null) {
                mp.stop();
            }
            stopButton.setVisibility(View.INVISIBLE);
            setOldBackground(currentButton);
        });
        updateUi(filter);
    }

    public void updateUi(String filter) {
        filter = filter.toUpperCase();
        File file = new File(getApplicationContext().getFilesDir() + "/" + folderName);
        String[] files = file.list((current, name) -> new File(current, name).isFile());
        LinearLayout layoutWhereAddBtn = findViewById(R.id.layoutFolder);
        layoutWhereAddBtn.removeAllViews();
        for (String s: files) {
            if (s.toUpperCase().contains(filter)) {
                Button btn = createButton(s, this);
                btn.setTextSize(14);
                btn.setGravity(Gravity.LEFT);
                btn.setGravity(Gravity.CENTER_VERTICAL);
                btn.setPadding(10,0,10,0);
                btn.setOnClickListener(view -> {
                    setOldBackground(currentButton);
                    currentButton = btn;
                    btn.setBackgroundColor(Color.GREEN);
                    audioPlayer(file.getPath(), s, btn);
                });
                layoutWhereAddBtn.addView(btn);
            }
        }
    }

    public void audioPlayer(String path, String fileName, Button btn){
        if (mp != null) {
            mp.release();
        }
        mp = new MediaPlayer();
        mp.setOnCompletionListener(myMediaPlayer1 -> {
            stopButton.setVisibility(View.INVISIBLE);
            setOldBackground(btn);
        });
        try {
            mp.setDataSource(path + File.separator + fileName);
            mp.prepare();
            mp.start();
            stopButton.setVisibility(View.VISIBLE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        mp.release();
        setOldBackground(currentButton);
        stopButton.setVisibility(View.INVISIBLE);
    }

}

