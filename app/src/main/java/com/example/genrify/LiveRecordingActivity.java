package com.example.genrify;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;

import java.io.File;
import java.io.IOException;

public class LiveRecordingActivity extends BaseActivity implements View.OnClickListener {
    private static final String TAG = "LiveRecord";
    private Button startButton;
    private Button stopButton;
    private ProgressBar loader;
    private MediaRecorder mediaRecorder;
    private File audioFile;
    private TextView textViewAnalysing;
    private long startedMillis;
    private long endedMillis;
    private CountDownTimer cdt;
    private Button btnAnalyze;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.live_recording);
        this.loader = findViewById(R.id.loader);
        loader.setVisibility(View.INVISIBLE);
        this.textViewAnalysing = findViewById(R.id.textViewAnalysing);
        textViewAnalysing.setVisibility(View.INVISIBLE);
        this.btnAnalyze = findViewById(R.id.btnAnalyzeRecord);
        btnAnalyze.setVisibility(View.INVISIBLE);
        btnAnalyze.setOnClickListener(this);

        startButton = (Button) findViewById(R.id.btnStartRecording);
        startButton.setOnClickListener(this);

        stopButton = (Button) findViewById(R.id.btnStopRecording);
        stopButton.setOnClickListener(this);

        audioFile = new File(getApplicationContext().getFilesDir() + "/recordedaudio.mp3");
        audioFile.delete();
    }

    // this process must be done prior to the start of recording
    private void resetRecorder() {
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
        mediaRecorder.setAudioEncodingBitRate(16);
        mediaRecorder.setAudioSamplingRate(22050);
        mediaRecorder.setOutputFile(audioFile.getAbsolutePath());

        try {
            mediaRecorder.prepare();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnStartRecording:
                this.btnAnalyze.setVisibility(View.INVISIBLE);
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO)
                        != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(this, new String[] { Manifest.permission.RECORD_AUDIO },
                            10);
                } else {
                    recordAudio();
                }
                break;
            case R.id.btnStopRecording:
                stopRecord();
                break;
            case R.id.btnAnalyzeRecord:
                detectGenreFromFile();
        }
    }

    public void recordAudio(){
        Log.d(TAG, audioFile.getAbsolutePath());
        audioFile.delete();
        final int[] total = {0};

        this.loader.setVisibility(View.VISIBLE);
        this.textViewAnalysing.setVisibility(View.VISIBLE);
        mediaRecorder = new MediaRecorder();
        resetRecorder();
        this.startedMillis = System.currentTimeMillis();
        mediaRecorder.start();

        loader = (ProgressBar) findViewById(R.id.loader);
        loader.setProgress(total[0]);
        int trenteSec= 1 * 30 * 1000; // 30 sec in milli seconds

        /** CountDownTimer starts with 3 seconds and every onTick is 1 second */
        cdt = new CountDownTimer(trenteSec, 1000) {

            public void onTick(long millisUntilFinished) {

                //forward progress
                long finishedSeconds = trenteSec - millisUntilFinished;
                int total = (int) (((float)finishedSeconds / (float)trenteSec) * 100.0);
                Log.d(TAG, String.valueOf(total));
                loader.setProgress(total);
            }

            public void onFinish() {
                // DO something when 1 minute is up
                loader.setProgress(100);
                stopRecord();
            }
        }.start();

        startButton.setEnabled(false);
        stopButton.setEnabled(true);
    }

    public void stopRecord(){
        this.loader.setVisibility(View.INVISIBLE);
        this.textViewAnalysing.setVisibility(View.INVISIBLE);
        mediaRecorder.stop();
        cdt.cancel();
        this.endedMillis = System.currentTimeMillis();
        mediaRecorder.release();
        mediaRecorder = null;

        startButton.setEnabled(true);
        stopButton.setEnabled(false);

        if(endedMillis - startedMillis < 30000){
            Toast.makeText(this,R.string.recordTooShort,Toast.LENGTH_SHORT).show();
        }else{
            this.btnAnalyze.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (mediaRecorder != null) {
            mediaRecorder.stop();
            cdt.cancel();
            mediaRecorder.release();
            mediaRecorder = null;
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                                     @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 10) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                recordAudio();
            }else{
                //User denied Permission.
            }
        }
    }

    private void detectGenreFromFile() {
        MainActivity.mp3PickedUri = Uri.parse(audioFile.toURI().toString());
        MainActivity.mp3PickedName = this.getNameFromURI(MainActivity.mp3PickedUri);
        Intent i = new Intent(this, AnalysingActivity.class);
        startActivity(i);
    }

}

