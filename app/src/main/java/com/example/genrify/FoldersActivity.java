package com.example.genrify;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.SearchView;

import java.io.File;

public class FoldersActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.folders);
        SearchView search = findViewById(R.id.searchFolders);
        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                updateUi(newText);
                return true;
            }
        });
        String filter = search.getQuery().toString();
        updateUi(filter);
    }

    public void updateUi (String filter) {
        filter = filter.toUpperCase();
        File file = new File(String.valueOf(getApplicationContext().getFilesDir()));
        String[] directories = file.list((current, name) -> new File(current, name).isDirectory());
        LinearLayout layoutWhereAddBtn = findViewById(R.id.layoutFolders);
        layoutWhereAddBtn.removeAllViews();
        for (String s: directories) {
            if (s.toUpperCase().contains(filter)) {
                Button btn = createButton(s, this);
                btn.setOnClickListener(view -> {
                    Intent i = new Intent(this, FolderActivity.class);
                    i.putExtra("text", s);
                    startActivity(i);
                });
                layoutWhereAddBtn.addView(btn);
            }
        }
    }

}

