package com.example.genrify;

import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class HistoryActivity extends BaseActivity {

    private static final String TAG = "HistoryAct";
    private Button stopButton;
    private Map<Long,File> files;
    private LinearLayout fileListLayout;
    private SimpleDateFormat timeFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
    private Button currentButton;
    private MediaPlayer mp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        stopButton = findViewById(R.id.stopMusic);
        stopButton.setVisibility(View.INVISIBLE);
        stopButton.setOnClickListener(view -> {
            if (mp != null) {
                mp.stop();
            }
            stopButton.setVisibility(View.INVISIBLE);
            setOldBackground(currentButton);
        });

        fileListLayout = findViewById(R.id.layoutHistory);
        fileListLayout.removeAllViews();

        this.files = new TreeMap<>(Collections.reverseOrder());
        this.listf(getApplicationContext().getFilesDir().getAbsolutePath(), files);

        Set set = files.entrySet();
        Iterator i = set.iterator();
        // Traverse map and print elements
        while (i.hasNext()) {
            Map.Entry me = (Map.Entry)i.next();
            Long dateFile = (Long) me.getKey();
            File file = (File) me.getValue();
            Button btn = createButton(timeFormat.format(dateFile) + " | " + file.getName(), this);
            btn.setTextSize(14);
            btn.setGravity(Gravity.LEFT);
            btn.setGravity(Gravity.CENTER_VERTICAL);
            btn.setPadding(10,0,10,0);
            btn.setOnClickListener(view -> {
                setOldBackground(currentButton);
                currentButton = btn;
                btn.setBackgroundColor(Color.GREEN);
                audioPlayer(file.getParent(), file.getName(), btn);
            });
            fileListLayout.addView(btn);
        }
    }

    public void audioPlayer(String path, String fileName, Button btn){
        if (mp != null) {
            mp.release();
        }
        mp = new MediaPlayer();
        mp.setOnCompletionListener(myMediaPlayer1 -> {
            stopButton.setVisibility(View.INVISIBLE);
            setOldBackground(btn);
        });
        try {
            mp.setDataSource(path + File.separator + fileName);
            mp.prepare();
            mp.start();
            stopButton.setVisibility(View.VISIBLE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(mp != null){
            mp.release();
        }
        setOldBackground(currentButton);
        stopButton.setVisibility(View.INVISIBLE);
    }

    public void listf(String directoryName, Map<Long,File> files) {
        Log.d(TAG, directoryName);
        File directory = new File(directoryName);

        // Get all files from a directory.
        File[] fList = directory.listFiles();
        if(fList != null) {
            for (File file : fList) {
                if (file.isFile()) {
                    files.put(file.lastModified(), file);
                } else if (file.isDirectory()) {
                    listf(file.getAbsolutePath(), files);
                }
            }
        }
    }
}