package com.example.genrify;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.SearchView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;

import java.util.HashMap;
import java.util.Map;

public class ContestActivity extends BaseActivity {

    private static final String TAG = "ContestAct";
    private String id_token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contest);
        SearchView search = findViewById(R.id.searchContest);
        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                updateUi(newText);
                return true;
            }
        });
        String filter = search.getQuery().toString();
        updateUi(filter);

        this.getCurrentUser().getIdToken(true).addOnSuccessListener(getTokenResult -> id_token = getTokenResult.getToken());
    }

    public void updateUi(String filter) {
        filter = filter.toUpperCase();
        LinearLayout layoutWhereAddBtn = findViewById(R.id.layoutContest);
        layoutWhereAddBtn.removeAllViews();
        String mfcc = AnalysingActivity.mfcctoshare;
        for (String s: allGenres) {
            if(s.contains(filter)) {
                Button btn = createButton(s, this);
                btn.setOnClickListener(view -> {
                    RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                    JSONArray params = new JSONArray();
                    try {
                        params.put(0, mfcc);
                        params.put(1, s.toLowerCase());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    JsonArrayRequest postRequest = new JsonArrayRequest(Request.Method.POST, urlRectify, params,
                            response -> Log.d(TAG, response.toString()),
                            error -> Log.d(TAG, error.toString())
                    ) {
                        @Override
                        public Map<String, String> getHeaders() {
                            HashMap<String, String> headers = new HashMap<String, String>();
                            headers.put("Content-Type", "application/json; charset=utf-8");
                            headers.put("Authorization", id_token);
                            return headers;
                        }
                    };
                    postRequest.setRetryPolicy(new DefaultRetryPolicy(50000, 5, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    requestQueue.add(postRequest);

                    AnalysingEndActivity.saveMp3(s, getApplicationContext());
                    Intent i = new Intent(this, MainActivity.class);
                    startActivity(i);
                });
                layoutWhereAddBtn.addView(btn);
            }
        }
    }
}

