package com.example.genrify;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.widget.Button;
import android.widget.Toast;

import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.FirebaseAuthUIActivityResultContract;
import com.firebase.ui.auth.IdpResponse;
import com.firebase.ui.auth.data.model.FirebaseAuthUIAuthenticationResult;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends BaseActivity {

    private static final String TAG = "MainAct";
    private static final int REQUEST_READ_PERMISSION = 1;
    private static final int MP3_PICKED_EVENT = 100;

    public static Uri mp3PickedUri;
    public static String mp3PickedName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        setUpHomeActivity();
    }

    private Button getBtn(int id) {
        return findViewById(id);
    }

    private void setUpHomeActivity() {
        setContentView(R.layout.home);
        getBtn(R.id.btnDetectGenreLive).setOnClickListener(view -> {
            Intent i = new Intent(this, LiveRecordingActivity.class);
            startActivity(i);
        });
        getBtn(R.id.btnFolders).setOnClickListener(view -> {
            Intent i = new Intent(this, FoldersActivity.class);
            startActivity(i);
        });
        getBtn(R.id.btnDetectGenreFile).setOnClickListener(view -> {
            checkPermission(Manifest.permission.READ_EXTERNAL_STORAGE, REQUEST_READ_PERMISSION);
        });
        getBtn(R.id.btnHistoric).setOnClickListener(view -> {
            Intent i = new Intent(this, HistoryActivity.class);
            startActivity(i);
        });
        getBtn(R.id.btnLogin).setOnClickListener(view -> {
            loginUser();
        });

        if(this.isCurrentUserLogged()){
            getBtn(R.id.btnLogin).setText(R.string.strlogout);
            getBtn(R.id.btnLogin).setOnClickListener(view -> {
                logoutUser();
            });
        }

        Intent intent = getIntent();

        if (intent.hasExtra("ErrorAPI")) {
            Toast.makeText(this, R.string.errorAPI, Toast.LENGTH_SHORT).show();
        }
    }

    private void detectGenreFromFile() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("audio/mpeg");
        startActivityForResult(intent, MP3_PICKED_EVENT);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != Activity.RESULT_OK) {
            Log.d(TAG,"Error in onActivityResult with requestCode = " + requestCode);
            return;
        }
        if (requestCode == MP3_PICKED_EVENT){
            if ((data != null) && (data.getData() != null)){
                MainActivity.mp3PickedUri = data.getData();
                MainActivity.mp3PickedName = this.getNameFromURI(mp3PickedUri);
                Intent i = new Intent(this, AnalysingActivity.class);
                startActivity(i);
            }
        }
    }

    private void loginUser(){
        // Choose authentication providers
        List<AuthUI.IdpConfig> providers = Arrays.asList(
                new AuthUI.IdpConfig.EmailBuilder().build(),
                new AuthUI.IdpConfig.GoogleBuilder().build());

        // Create and launch sign-in intent
        Intent signInIntent = AuthUI.getInstance()
                .createSignInIntentBuilder()
                .setAvailableProviders(providers)
                .build();
        signInLauncher.launch(signInIntent);
    }
    private void logoutUser(){
        MainActivity context = this;
        AuthUI.getInstance()
                .signOut(context)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    public void onComplete(@NonNull Task<Void> task) {
                        Intent i = new Intent(context, MainActivity.class);
                        startActivity(i);
                        finish();
                    }
                });
    }

    // Function to check and request permission.
    public void checkPermission(String permission, int requestCode)
    {
        if (ContextCompat.checkSelfPermission(MainActivity.this, permission) == PackageManager.PERMISSION_DENIED) {
            // Requesting the permission
            ActivityCompat.requestPermissions(MainActivity.this, new String[] { permission }, requestCode);
        }
        else {
            Log.d(TAG, "Permission already granted");
            detectGenreFromFile();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_READ_PERMISSION:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 &&
                        grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission is granted. Continue the action or workflow
                    // in your app.
                } else {
                    // Explain to the user that the feature is unavailable because
                    // the features requires a permission that the user has denied.
                    // At the same time, respect the user's decision. Don't link to
                    // system settings in an effort to convince the user to change
                    // their decision.
                }
                return;
        }
        // Other 'case' lines to check for other
        // permissions this app might request.
    }

    private final ActivityResultLauncher<Intent> signInLauncher = registerForActivityResult(
            new FirebaseAuthUIActivityResultContract(),
            new ActivityResultCallback<FirebaseAuthUIAuthenticationResult>() {
                @Override
                public void onActivityResult(FirebaseAuthUIAuthenticationResult result) {
                    onSignInResult(result);
                }
            }
    );
    private void onSignInResult(FirebaseAuthUIAuthenticationResult result) {
        IdpResponse response = result.getIdpResponse();
        if (result.getResultCode() == RESULT_OK) {
            // Successfully signed in
            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            // ...
            Intent i = new Intent(this, MainActivity.class);
            startActivity(i);
        } else {
            // Sign in failed. If response is null the user canceled the
            // sign-in flow using the back button. Otherwise check
            // response.getError().getErrorCode() and handle the error.
            // ...
        }
    }
}

