package com.example.genrify;

import android.app.Activity;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.OpenableColumns;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import org.jetbrains.annotations.NotNull;

public class BaseActivity extends AppCompatActivity {

    public static final String[] allGenres = {"ROCK", "POP", "BLUES", "JAZZ", "CLASSICAL", "REGGAE", "COUNTRY", "DISCO", "HIPHOP", "METAL"};
    private TextView credentialsAndRole;
    private static final String TAG = "BaseAct";
    public static final String urlGetGenre = "https://recette.teamber.fr/teamberapi_graph/genre2";
    public static final String urlRectify = "https://recette.teamber.fr/teamberapi_graph/rectify";


    @Nullable
    protected FirebaseUser getCurrentUser(){ return FirebaseAuth.getInstance().getCurrentUser(); }

    protected Boolean isCurrentUserLogged(){ return (this.getCurrentUser() != null); }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (this.isCurrentUserLogged()) {
            this.credentialsAndRole = findViewById(R.id.tvCredentialsAndRole);
            String text = getResources().getString(R.string.credentialsAndRole, this.getCurrentUser().getDisplayName());
            this.credentialsAndRole.setText(text);
        }
    }

    public String getNameFromURI(Uri uri) {
        String r = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    r = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (r == null) {
            r = uri.getPath();
            int cut = r.lastIndexOf('/');
            if (cut != -1) {
                r = r.substring(cut + 1);
            }
        }
        return r;
    }

    @NotNull
    public Button createButton(String s, Activity a) {
        Button btn  = new Button(a);
        btn.setText(s);
        btn.setTextSize(24);
        btn.setTextColor(Color.WHITE);
        TypedValue typedValue = new TypedValue();
        a.getTheme().resolveAttribute(R.attr.colorPrimary, typedValue, true);
        btn.setBackgroundColor(typedValue.data);
        //btn.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        btn.setHeight(30);
        btn.setWidth(200);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT
        );
        params.setMargins(10, 10, 10, 10);
        params.gravity = Gravity.CENTER;
        btn.setLayoutParams(params);
        return btn;
    }

    public void setOldBackground(Button btn) {
        if (btn != null) {
            TypedValue typedValue = new TypedValue();
            this.getTheme().resolveAttribute(R.attr.colorPrimary, typedValue, true);
            btn.setBackgroundColor(typedValue.data);
        }
    }
}