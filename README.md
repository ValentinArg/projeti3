# Projeti3

ARGENTY Valentin /
BARATA Frédéric /
DIEZ Roman /
GONZALEZ Julien

# Descriptif du sujet

Notre système se base sur l’analyse d’extraits musicaux pour définir leur genre grâce à de l’intelligence artificielle. Les utilisateurs auront à disposition une application mobile qui, à l’instar d’un Shazam, pourra capter le son pour l’enregistrer puis l’analyser et donner le genre en quelques instants. De plus, il sera possible d’intégrer un fichier déjà présent sur l’appareil pour l’analyser et donner son genre puis l’enregistrer pour de futures consultations. Les extraits qui ont été analysés par l’utilisateur seront stockés sur l’appareil puis classés selon le genre qui aura été défini. Pour ce qui est de l’analyse, elle se fera grâce à un service hébergé sur le Cloud, ce qui aidera à stocker les extraits au fur et à mesure pour améliorer progressivement le modèle.  
Les acteurs du système sont les suivants: les utilisateurs qui utilisent l’application, l’appareil mobile qui capte le son ou importe un fichier, le service Cloud pour l’IA (par exemple TensorFlow de Google pour les réseaux de neurones), l’appareil qui émet le son à analyser.  
Les données d’entrées du système sont: le son capté par un micro ainsi que des fichiers audio, et les données en sorties représenteront le genre musical estimé de l’extrait.  
  
En plus de ce fonctionnement, certains utilisateurs avec des comptes spécifiques auront la possibilité de contester les estimations afin d'indiquer le genre qu'ils pensent être le bon dans le but d'améliorer l'IA.
